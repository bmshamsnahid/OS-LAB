package com.ShamsNahid.MemoryAllocation;

/**
 * Created by bmshamsnahid on 4/14/17.
 */
public class Process {

    private int processNo;
    private int processSize;

    public int getProcessNo() {
        return processNo;
    }

    public void setProcessNo(int processNo) {
        this.processNo = processNo;
    }

    public int getProcessSize() {
        return processSize;
    }

    public void setProcessSize(int processSize) {
        this.processSize = processSize;
    }
}
