package com.ShamsNahid.MemoryAllocation;

/**
 * Created by bmshamsnahid on 4/14/17.
 */
public class MemoryBlock {

    private int memoryBlockNo;
    private int memoryBlockSize;
    private Boolean isAllocated;

    public int getMemoryBlockNo() {
        return memoryBlockNo;
    }

    public void setMemoryBlockNo(int memoryBlockNo) {
        this.memoryBlockNo = memoryBlockNo;
    }

    public int getMemoryBlockSize() {
        return memoryBlockSize;
    }

    public void setMemoryBlockSize(int memoryBlockSize) {
        this.memoryBlockSize = memoryBlockSize;
    }

    public Boolean getAllocated() {
        return isAllocated;
    }

    public void setAllocated(Boolean allocated) {
        isAllocated = allocated;
    }
}
