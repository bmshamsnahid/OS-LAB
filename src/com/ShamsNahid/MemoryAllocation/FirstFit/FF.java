package com.ShamsNahid.MemoryAllocation.FirstFit;

import com.ShamsNahid.MemoryAllocation.MemoryBlock;
import com.ShamsNahid.MemoryAllocation.Process;
import com.ShamsNahid.MemoryAllocation.Utility;

import java.util.ArrayList;

/**
 * Created by bmshamsnahid on 4/14/17.
 * Input
 3
 300
 200
 400
 3
 100
 150
 250
 */
public class FF {

    public static void main(String[] args) {

        Utility utility = new Utility();

        ArrayList<MemoryBlock> alMemoryBlock = new ArrayList<>();
        ArrayList<Process> alProcess = new ArrayList<>();

        utility.gotMemoryBlockAndProcess(alMemoryBlock, alProcess);
        utility.DisplayInitializeInfo(alMemoryBlock, alProcess);
        utility.memoryAllocation(alMemoryBlock, alProcess);

    }
}
