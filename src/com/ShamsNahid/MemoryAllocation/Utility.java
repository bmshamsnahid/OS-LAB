package com.ShamsNahid.MemoryAllocation;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by bmshamsnahid on 4/14/17.
 */
public class Utility {

    public void gotMemoryBlockAndProcess(ArrayList<MemoryBlock> alMemoryBlock, ArrayList<Process> alProcess) {
        Scanner scanner = new Scanner(System.in);

        int noOfMemoryBlock;
        int noOfProcess;

        System.out.println("No of memory block: ");
        noOfMemoryBlock = scanner.nextInt();

        for(int index=1; index<=noOfMemoryBlock; index++) {
            int memoryBlockNo = index;
            int memoryBlockSize = scanner.nextInt();
            Boolean memoryAllocated = false;

            MemoryBlock memoryBlock = new MemoryBlock();
            memoryBlock.setMemoryBlockNo(memoryBlockNo);
            memoryBlock.setMemoryBlockSize(memoryBlockSize);
            memoryBlock.setAllocated(memoryAllocated);

            alMemoryBlock.add(memoryBlock);
        }

        System.out.println("No of process: ");
        noOfProcess = scanner.nextInt();

        for(int index=1; index<=noOfProcess; index++) {
            int processNo = index;
            int processSize = scanner.nextInt();

            Process process = new Process();
            process .setProcessNo(processNo);
            process .setProcessSize(processSize);

            alProcess.add(process);
        }
    }

    public void DisplayInitializeInfo(ArrayList<MemoryBlock> alMemoryBlock, ArrayList<Process> alProcess) {
        System.out.println("MEMORY BLOCK INFORMATION");
        for(MemoryBlock mb : alMemoryBlock) {
            System.out.println("no: " + mb.getMemoryBlockNo());
            System.out.println("size: " + mb.getMemoryBlockSize());
            System.out.println("status: " + mb.getAllocated());
        }
        System.out.println("PROCESS INITIALIZE INFO");
        for(Process process : alProcess) {
            System.out.println("process no: " + process.getProcessNo());
            System.out.println("size: " + process.getProcessSize());
        }
    }

    public void memoryAllocation(ArrayList<MemoryBlock> alMemoryBlock, ArrayList<Process> alProcess) {
        for(Process process : alProcess) {
            Boolean gotMemoryBlock = false;
            for(MemoryBlock mb : alMemoryBlock) {
                if(mb.getMemoryBlockSize() >= process.getProcessSize() && mb.getAllocated() == false) {
                    mb.setAllocated(true);
                    System.out.println("Process " + process.getProcessNo() + " processSize: " + process.getProcessSize() + " MemoryBlockNo: " + mb.getMemoryBlockNo() + " MemeoryBlockSize: " + mb.getMemoryBlockSize() + "  IF: " + (mb.getMemoryBlockSize() - process.getProcessSize()));
                    gotMemoryBlock = true;
                    break;
                }
            }
            if(gotMemoryBlock == false) {
                System.out.println("Process " + process.getProcessNo() + " processSize: " + process.getProcessSize() + " NO MEMORY BLOCK IS SUITABLE FOR THIS PROCESS");
            }
        }
    }

}
