package com.ShamsNahid.MemoryAllocation.WorstFit;

import com.ShamsNahid.MemoryAllocation.MemoryBlock;
import com.ShamsNahid.MemoryAllocation.Process;
import com.ShamsNahid.MemoryAllocation.Utility;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by bmshamsnahid on 4/14/17.
 * Input
 3
 300
 200
 400
 3
 100
 150
 250
 */

public class WF {

    public static void main(String[] args) {

        Utility utility = new Utility();

        ArrayList<MemoryBlock> alMemoryBlock = new ArrayList<>();
        ArrayList<Process> alProcess = new ArrayList<>();

        utility.gotMemoryBlockAndProcess(alMemoryBlock, alProcess);
        sortDecending(alMemoryBlock);   //for best fit sorting
        utility.DisplayInitializeInfo(alMemoryBlock, alProcess);
        utility.memoryAllocation(alMemoryBlock, alProcess);

    }

    private static void sortDecending(ArrayList<MemoryBlock> alMemoryBlock) {

        for(int index1=0; index1<alMemoryBlock.size(); index1++) {
            for(int index2=0; index2<alMemoryBlock.size(); index2++) {
                MemoryBlock mb1 = alMemoryBlock.get(index1);
                MemoryBlock mb2 = alMemoryBlock.get(index2);
                if(mb1.getMemoryBlockSize() > mb2.getMemoryBlockSize()) {
                    Collections.swap(alMemoryBlock, index1, index2);
                }
            }
        }

    }
}