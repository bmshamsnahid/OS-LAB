package com.ShamsNahid.PageReplacement;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by bmshamsnahid on 4/14/17.
 * input
 3
 14
 4 7 3 0 1 7 3 8 5 4 5 3 4 7
 */
public class FIFO {

    public static void main(String[] args) {
        Utility utility = new Utility();

        ArrayList<Page> alPagese = new ArrayList<>();
        ArrayList<Page> alMemory = new ArrayList<>();

        int memorySize;
        int noOfPages;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Memory size: ");
        memorySize = scanner.nextInt();
        System.out.println("No Of pages: ");
        noOfPages = scanner.nextInt();

        utility.getMemorySizeAndPages(alPagese, noOfPages);

        utility.displayPages(alPagese, "test");

        for(int index=0; index<alPagese.size(); index++) {
            //page is not in the memory, page fault
            if(utility.isContained(alMemory, alPagese.get(index)) == false) {
                //System.out.println("size : "  + alMemory.size());
                if(alMemory.size() < memorySize) {
                    alMemory.add(alPagese.get(index));
                } else {
                    int oldPageIndex = utility.oldpageIndexFinder(alMemory);
                    //System.out.println("oldIndex " + oldPageIndex);
                    alMemory.set(oldPageIndex, alPagese.get(index));
                }
                utility.increaseTime(alMemory);
                utility.displayPages(alMemory, " Page Fault");
            } else {
                utility.displayPages(alMemory, " ");
            }

        }

    }
}
