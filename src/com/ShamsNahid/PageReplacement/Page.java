package com.ShamsNahid.PageReplacement;

/**
 * Created by bmshamsnahid on 4/14/17.
 */
public class Page {

    private int pageNo;
    private int pageTime;
    private int pageOccourance;
    private int nextOccourance;

    public int getNextOccourance() {
        return nextOccourance;
    }

    public void setNextOccourance(int nextOccourance) {
        this.nextOccourance = nextOccourance;
    }

    public int getPageOccourance() {
        return pageOccourance;
    }

    public void setPageOccourance(int pageOccourance) {
        this.pageOccourance = pageOccourance;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageTime() {
        return pageTime;
    }

    public void setPageTime(int pageTime) {
        this.pageTime = pageTime;
    }
}
