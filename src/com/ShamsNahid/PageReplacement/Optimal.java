package com.ShamsNahid.PageReplacement;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by bmshamsnahid on 4/14/17.
 input
 3
 9
 0 4 3 2 1 4 6 3 0
 */
public class Optimal {

    public static void main(String[] args) {
        Utility utility = new Utility();

        ArrayList<Page> alPagese = new ArrayList<>();
        ArrayList<Page> alMemory = new ArrayList<>();

        int memorySize;
        int noOfPages;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Memory size: ");
        memorySize = scanner.nextInt();
        System.out.println("No Of pages: ");
        noOfPages = scanner.nextInt();

        utility.getMemorySizeAndPages(alPagese, noOfPages);

        utility.displayPages(alPagese, "test");

        for(int index=0; index<alPagese.size(); index++) {
            //page is not in the memory, page fault
            if(utility.isContained(alMemory, alPagese.get(index)) == false) {
                if(alMemory.size() < memorySize) {
                    alMemory.add(alPagese.get(index));
                } else {
                    int nextLateOccouranceIndex = utility.nextLateOccouranceFinder(alMemory, alPagese, index);
                    //System.out.println("oldIndex " + oldPageIndex);
                    alMemory.set(nextLateOccouranceIndex, alPagese.get(index));
                }
                utility.increaseTime(alMemory);
                utility.increaseOccourance(alMemory);
                utility.displayPages(alMemory, " Page Fault");
            } else {
                utility.displayPages(alMemory, " ");
            }

        }
    }

}
