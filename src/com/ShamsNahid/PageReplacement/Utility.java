package com.ShamsNahid.PageReplacement;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by bmshamsnahid on 4/14/17.
 *
 */
public class Utility {

    public void getMemorySizeAndPages(ArrayList<Page> alPages, int noOfPages) {
        Scanner scanner = new Scanner(System.in);

        for(int index=0; index<noOfPages; index++) {
            int pageNo = scanner.nextInt();
            int pageTime = 0;
            int pageOccourance = 0;
            int nextOccourance = 1000;

            Page page = new Page();
            page.setPageNo(pageNo);
            page.setPageTime(pageTime);
            page.setPageOccourance(pageOccourance);
            page.setNextOccourance(nextOccourance);

            alPages.add(page);
        }
    }

    public boolean isContained(ArrayList<Page> alMemory, Page page) {
        for(Page memoryPage : alMemory) {
            if(memoryPage.getPageNo() == page.getPageNo()) return true;
        }
        return false;
    }

    public int oldpageIndexFinder(ArrayList<Page> alMemory) {

        int oldPageIndex = 0;
        int maxTime = -1;

        for(int index=0; index<alMemory.size(); index++) {
            Page page = alMemory.get(index);
            if(page.getPageTime() > maxTime) {
                oldPageIndex = index;
                maxTime = page.getPageTime();
            }
        }

        //System.out.println("oldPage: " + alMemory.get(oldPageIndex).getPageNo() + " time: " + alMemory.get(oldPageIndex).getPageTime());

        return oldPageIndex;

    }

    public int leastPageOccouranceFinder(ArrayList<Page> alMemory) {

        int lestPageOccouranceIndex = 0;
        int pageOccourance = 1000;
        int maxTime = -1;

        for(int index=0; index<alMemory.size(); index++) {
            Page page = alMemory.get(index);

            if(page.getPageOccourance() < pageOccourance) {
                pageOccourance = page.getPageOccourance();
                lestPageOccouranceIndex = index;
            }
        }

        return lestPageOccouranceIndex;

    }

    public void increaseTime(ArrayList<Page> alMemory) {
        for(Page page : alMemory) {
            page.setPageTime(page.getPageTime() + 1);
        }
    }

    public void increaseOccourance(ArrayList<Page> alMemory) {
        for(Page page : alMemory) {
            page.setPageTime(page.getPageOccourance() + 1);
        }
    }

    public void displayPages(ArrayList<Page> alPages, String message) {
        //System.out.println("Pages are: ");
        for(Page page : alPages) {
            System.out.print(page.getPageNo() + " ");
        }
        System.out.print(message);
        System.out.println();
    }

    public int nextLateOccouranceFinder(ArrayList<Page> alMemory, ArrayList<Page> alPages, int startPoint) {

        int index = 0;
        int maxLength = -1;

        for(int index1=0; index1<alMemory.size(); index1++) {
            int count = 0;
            for(int index2=startPoint; index2<alPages.size(); index2++) {
                count ++;
                Page memPage = alMemory.get(index1);
                Page listPage = alPages.get(index2);

                if(memPage.getPageNo() == listPage.getPageNo()) {
                    memPage.setNextOccourance(count);
                    break;
                }
            }

            if(alMemory.get(index1).getNextOccourance() >= maxLength) {
                maxLength = alMemory.get(index1).getNextOccourance();
                index = index1;
            }
        }

        return index;

    }
}
