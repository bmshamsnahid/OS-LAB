package com.ShamsNahid.bankers;

import java.util.ArrayList;
import java.util.Scanner;

//input
/*
 5

 10 5 7

 0 1 0    7 5 3
 2 0 0    3 2 2
 3 0 2    9 0 2
 2 1 1    2 2 2
 0 0 2    4 3 3

Deadlock
 5

 10 5 7

 0 1 0    7 5 3
 2 0 0    3 2 2
 3 0 2    19 0 2
 2 1 1    2 2 2
 0 0 2    4 3 3
* */


public class BankersAlgorithm {
	
	public static void main(String[] args) {
		ArrayList<Process> alProcess = new ArrayList<>();
		
		int instanceA, instanceB, instanceC;
		int totalA = 0, totalB = 0, totalC = 0;
		
		int noOfProcess;
		
		Scanner scanner = new Scanner(System.in);
		
		noOfProcess = scanner.nextInt();
		
		instanceA = scanner.nextInt();
		instanceB = scanner.nextInt();
		instanceC = scanner.nextInt();
		
		for(int index=0; index<noOfProcess; index++) {
			Process process = new Process();
			ArrayList<Integer> tmpAl = new ArrayList<>();
			ArrayList<Integer> tmpMx = new ArrayList<>();
			ArrayList<Integer> tmpNeed = new ArrayList<>();
			
			for(int i=0; i<3; i++) {
				int val = scanner.nextInt();
				tmpAl.add(val);
			}
			for(int i=0; i<3; i++) {
				int val = scanner.nextInt();
				tmpMx.add(val);
				tmpNeed.add(val - tmpAl.get(i));
			}
			
			process.setAllocation(tmpAl);
			process.setMax(tmpMx);
			process.setNeed(tmpNeed);
			process.setStatus(false);
			process.setName("p" + String.valueOf(index));
			alProcess.add(process);
		}
		
		
		for(Process ps : alProcess) {
			totalA += ps.getAllocation().get(0);
			totalB += ps.getAllocation().get(1);
			totalC += ps.getAllocation().get(2);
			ps.setStatus(false);
		}
		
		System.out.println("A: " + instanceA + " " + totalA + " B: " + instanceB + " " + totalB + " C: " + instanceC + " " + totalC);

		
		ArrayList<Integer> work = new ArrayList<>();
		work.add(instanceA - totalA);
		work.add(instanceB  - totalB);
		work.add(instanceC  - totalC);
		
		System.out.println("Work: " + work);
		
		ArrayList<Process> resultProcess = new ArrayList<>();
		
		for(Process parentProcess: alProcess) {
			for(Process ps: alProcess) {
				
				if((ps.getStatus() == false) && checkAvailability(ps, work)) {
					System.out.println(ps.getName() + " is available. " + ps.getAllocation() + " is smaller or equal to " + work);
					work = sum(ps, work);
					ps.setStatus(true);
					System.out.println("<<<<Work is: " + work);
					resultProcess.add(ps);
					ps.setAvailable(work);
				} else {
					System.out.println(ps.getName() + " is not available. status: " + ps.getStatus() + " " + ps.getAllocation() + " is greater than " + work);
				}
			}
		}
		
		showInput(alProcess);
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>ResultProcess: ");
		showResult(resultProcess, noOfProcess);
	}
	
	private static ArrayList<Integer> sum(Process ps, ArrayList<Integer> work) {
		ArrayList<Integer> newWork = new ArrayList<>();
		for(int index=0; index<3; index++) {
			newWork.add(work.get(index) + ps.getAllocation().get(index));
		}
		return newWork;
	}

	private static boolean checkAvailability(Process ps, ArrayList<Integer> work) {
		for(int index=0; index<3; index++) {
			if(ps.getNeed().get(index) > work.get(index)) {
				return false;
			}
		}
		return true;
	}

	private static void showInput(ArrayList<Process> alProcess) {
		System.out.println("All Process");
		for(Process process: alProcess) {
			System.out.println("Allocation: " + process.getAllocation());
			System.out.println("Max: " + process.getMax());
			System.out.println("Need: " + process.getNeed());
		}
	}
	
	private static void showResult(ArrayList<Process> alProcess, int noOfProcess) {
		System.out.println("All Process");
		boolean flag = true;
		for(Process process: alProcess) {
			System.out.print(process.getName() + " ");
			if(process.getStatus() == false ) flag = false;
		}
		System.out.println();
		for(Process process: alProcess) {
			System.out.println("Allocation: " + process.getAllocation());
			System.out.println("Max: " + process.getMax());
			System.out.println("Need: " + process.getNeed());
		}
		if(flag) {
			System.out.println("<<<<<<<<<<<<<<<<<<<<<<ALL TRUE>>>>>>>>>>>>>>>>>>>>>>>>>>");
		} else {
			System.out.println("<<<<<<<<<<<<<<<<<<<<<<ALL FALSE>>>>>>>>>>>>>>>>>>>>>>>>>>");
		}
		if(alProcess.size() != noOfProcess) {
            System.out.println("DEADLOCK OCCOURS");
        } else {
            System.out.println("NO DEADLOCK");
        }
		
		for(Process process: alProcess) {
			System.out.println(process.getName() + " " + process.getAvailable());
		}
	}
	
	
	

}
