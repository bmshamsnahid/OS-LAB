package com.ShamsNahid.bankers;

import java.util.ArrayList;

public class Process {
	
	private ArrayList<Integer> allocation;
	private ArrayList<Integer> max;
	private ArrayList<Integer> need;
	private ArrayList<Integer> available;
	private Boolean status;
	private String name;
	
	public ArrayList<Integer> getAllocation() {
		return allocation;
	}
	public void setAllocation(ArrayList<Integer> allocation) {
		this.allocation = allocation;
	}
	public ArrayList<Integer> getMax() {
		return max;
	}
	public void setMax(ArrayList<Integer> max) {
		this.max = max;
	}
	public ArrayList<Integer> getNeed() {
		return need;
	}
	public void setNeed(ArrayList<Integer> need) {
		this.need = need;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArrayList<Integer> getAvailable() {
		return available;
	}
	public void setAvailable(ArrayList<Integer> available) {
		this.available = available;
	}
	
}
