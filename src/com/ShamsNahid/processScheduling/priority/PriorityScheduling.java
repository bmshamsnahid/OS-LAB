package com.ShamsNahid.processScheduling.priority;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

import com.ShamsNahid.processScheduling.Process;

//input 5 10 3 1 1 2 3 1 4 5 2

public class PriorityScheduling {
	public static void main(String[] args) {
		Scanner scanner  = new Scanner(System.in);
		Process process;
		
		ArrayList<Process> alProcess = new ArrayList<>();
		
		System.out.print("No Of Process: ");
		int noOfProcess = scanner.nextInt();
		int index = 1;
		
		while((index <= noOfProcess)) {
			
			System.out.print("Burst Time: ");
			int bt = scanner.nextInt();
			int priority = scanner.nextInt();
			
			process = new Process();
			process.setProcessName("P" + String.valueOf(index));
			process.setArrivalTime(0);
			process.setBurstTime(bt);
			process.setPriority(priority);
			
			alProcess.add(process);
			
			index++;
		}
		
		showProcess(alProcess);
		alProcess = evaluateProcess(alProcess);
		System.out.println("--------------");
		showProcess(alProcess);
	}
	
	private static ArrayList<Process> evaluateProcess(ArrayList<Process> alProcess) {
		
		for(int index1=0; index1<alProcess.size(); index1++) {
			Process process1 = alProcess.get(index1);
			
			for(int index2=0; index2<alProcess.size(); index2++) {
				Process process2 = alProcess.get(index2);
				
				if(process1.getPriority() <= process2.getPriority()) {
					Collections.swap(alProcess, index1, index2);
				}
			}
		}
		
		showProcess(alProcess);
		
		for(int index=0; index<alProcess.size(); index++) {
			Process process = alProcess.get(index);
			
			if(index == 0) {
				process.setCompletionTime(process.getArrivalTime() + process.getBurstTime());
			} else {
				Process processPrev = alProcess.get(index-1);
				process.setCompletionTime(processPrev.getCompletionTime() + process.getBurstTime());
			}
			process.setTurnAroundTime(process.getCompletionTime() - process.getArrivalTime());
			process.setWaitingTime(process.getTurnAroundTime() - process.getBurstTime());
		}
		
		return alProcess;
	}
	
	
	private static void showProcess(ArrayList<Process> alProcess) {
		System.out.println("Process Information: ");
		System.out.println("PN\tPRT\tAT\tBT\tCT\tTAT\tWT");
		for(Process process : alProcess) {
			System.out.println(process.getProcessName() + "\t" + process.getPriority() + "\t" + process.getArrivalTime() + "\t" + process.getBurstTime() + "\t" + process.getCompletionTime() + "\t" + process.getTurnAroundTime() + "\t" + process.getWaitingTime());
		}
	}
}
