package com.ShamsNahid.processScheduling.fcfs;

import java.util.ArrayList;
import java.util.Scanner;

import com.ShamsNahid.processScheduling.Process;
//input 3 24 3 3 
public class FCFS {
	
	public static void main(String[] args) {
		Scanner scanner  = new Scanner(System.in);
		Process process;
		
		ArrayList<Process> alProcess = new ArrayList<>();
		
		System.out.print("No Of Process: ");
		int noOfProcess = scanner.nextInt();
		int index = 1;
		
		while((index <= noOfProcess)) {
			
			System.out.print("Burst Time: ");
			int bt = scanner.nextInt();
			
			process = new Process();
			process.setProcessName("P" + String.valueOf(index));
			process.setArrivalTime(0);
			process.setBurstTime(bt);
			
			alProcess.add(process);
			
			index++;
		}
		
		showProcess(alProcess);
		alProcess = evaluateProcess(alProcess);
		System.out.println("--------------");
		showProcess(alProcess);
	}
	
	private static ArrayList<Process> evaluateProcess(ArrayList<Process> alProcess) {
		
		for(int index=0; index<alProcess.size(); index++) {
			Process process = alProcess.get(index);
			
			if(index == 0) {
				process.setCompletionTime(process.getArrivalTime() + process.getBurstTime());
			} else {
				Process processPrev = alProcess.get(index-1);
				process.setCompletionTime(processPrev.getCompletionTime() + process.getBurstTime());
			}
			process.setTurnAroundTime(process.getCompletionTime() - process.getArrivalTime());
			process.setWaitingTime(process.getTurnAroundTime() - process.getBurstTime());
		}
		
		return alProcess;
	}

	private static void showProcess(ArrayList<Process> alProcess) {
		System.out.println("Process Information: ");
		System.out.println("PN\tAT\tBT\tCT\tTAT\tWT");
		for(Process process : alProcess) {
			System.out.println(process.getProcessName() + "\t" + process.getArrivalTime() + "\t" + process.getBurstTime() + "\t" + process.getCompletionTime() + "\t" + process.getTurnAroundTime() + "\t" + process.getWaitingTime());
		}
	}

}