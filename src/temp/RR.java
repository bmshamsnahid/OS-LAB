package temp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

import com.ShamsNahid.processScheduling.Process;

public class RR {
	
	public static void main(String[] args) {
		Scanner scanner  = new Scanner(System.in);
		Process process;
		
		ArrayList<Process> alProcess = new ArrayList<>();
		
		System.out.print("No Of Process: ");
		int noOfProcess = scanner.nextInt();
		int index = 1;
		
		while((index <= noOfProcess)) {
			
			System.out.print("Burst Time: ");
			int bt = scanner.nextInt();
			
			process = new Process();
			process.setProcessName("P" + String.valueOf(index));
			process.setArrivalTime(0);
			process.setBurstTime(bt);
			
			alProcess.add(process);
			
			index++;
		}
		
		alProcess = evaluateProcess(alProcess);
		showProcess(alProcess);
	}
	
	/*
	 * Here we declare a new process arrayList to keep the new sequence of the process execution
	 * A queue store the next process in the avail able time, i.e. one process is partially executed, then the next portion will be executed and kept in the queue 
	 * Initially we assume  the waiting time and the temp time is zero
	 * temp time is the last execution time of the CPU process
	 * tq is the input***
	 * Now while loop is running till the queue is running
	 * since running time is 0, all the process is in the queue
	 * Now we get the top queue process
	 * It's waiting time is the current time - the previous temp time
	 * Now then the process executed, that will kept in the process temp time
	 * Now each time if the remaining burst time is greater then the tq, then we execute the tq time and then set the new burst time
	 * else the burst time is remaining 0 and the current time is ct + remaining burst time
	 * When the process burst time is 0, i.e. process is completely executed and then the current time is the process completion time
	 * Also process tat is completion time - arrivalm time
	 * since process is completed, we remove the process from the arrayList
	*/
	private static ArrayList<Process> evaluateProcess(ArrayList<Process> alProcess) {
		ArrayList<Process> alNewProcess = new ArrayList<>();
		Queue<Process> qProcess = new LinkedList<>();
		
		for(Process process : alProcess) {
			process.setTempTime(0);
			process.setWaitingTime(0);
		}
		
		int tq = 2;
		int rt = 0;
		int ct = 0;
		
		do {
			queuedProcess(qProcess, rt, alProcess);
			
			Process process = qProcess.poll();
			
			Process ps = new Process();
			
			
			process.setWaitingTime(process.getWaitingTime() + (ct - process.getTempTime()));
			System.out.println(">>>>>>>>>>>>Process: " + process.getProcessName() + " And temp time: " + process.getTempTime() + " ct: " + ct + " waiting time: " + process.getWaitingTime());
			process.setTempTime(ct-process.getTempTime());
			
			
			if(process.getBurstTime() >= tq) {
				ct += tq;
			} else {
				ct += process.getBurstTime();
			}
			
			process.setBurstTime(((process.getBurstTime()-tq) < 0) ? 0 : (process.getBurstTime()-tq) );
			
			
			
			ps = process;
			ps.setTempTime(ct);
			alNewProcess.add(ps);
			
			if(process.getBurstTime()<= 0) {
				process.setCompletionTime(ct);
				process.setTurnAroundTime(process.getCompletionTime() - process.getArrivalTime());
				alProcess.remove(process);
			}
			
			System.out.println("Retrived Process: " + process.getProcessName());
			listProcessName(qProcess);
			listProcessName(alProcess);
			
		} while(!qProcess.isEmpty());
		
		System.out.println("Final:");
		listProcessName(alNewProcess);
		return alNewProcess;
	}
	
	/*
	 * Here we update the queue according to the process, available in the current time
	 * 
	*/
	private static Queue<Process> queuedProcess(Queue<Process> qProcess, int rt, ArrayList<Process> alProcess) {
		
		for(Process process : alProcess) {
			if(!qProcess.contains(process) && process.getArrivalTime() <= rt) {
				System.out.println("Adding");
				qProcess.add(process);
			}
		}
		
		return qProcess;
	}
	
	
	private static void showProcess(ArrayList<Process> alProcess) {
		System.out.println("Process Information: ");
		System.out.println("PN\tAT\tBT\tCT\tTAT\tWT");
		for(Process process : alProcess) {
			System.out.println(process.getProcessName() + "\t" + process.getArrivalTime() + "\t" + process.getBurstTime() + "\t" + process.getCompletionTime() + "\t" + process.getTurnAroundTime() + "\t" + process.getWaitingTime());
		}
	}
	
	private static void listProcessName(ArrayList<Process> alProcess) {
		System.out.print("Current ArrayList: ");
		for(Process process : alProcess) {
			System.out.print(process.getProcessName() + "(" + process.getCompletionTime() + ")" + "->");
		} System.out.println();
	}
	
	private static void listProcessName(Queue<Process> alProcess) {
		System.out.print("Current Queue: ");
		for(Process process : alProcess) {
			System.out.print(process.getProcessName() + "->");
		} System.out.println();
	}
}